## Huffman encoding [[toc](README.md#table-of-content)]

In this section, you will write a little tool that will (1) compress text using
_Huffman_ encoding and (2) decompress and reconstruct the original text, without
any loss. You will also measure the _efficiency_ of your algorithm.

Of course, the _Huffman_ encoding will use a tree to reduce the size of the
original text.

:file_folder: Create a file called `huffman.py`.

### Introduction [[toc](README.md#table-of-content)]

A text file is composed of symbols, e.g. ASCII or Unicode characters. It is thus
possible to
- define an alphabet of valid symbols
- count each symbol from the alphabet in the text
- compute the frequency of appearance of each symbol
- compute the information entropy of the text

The entropy is a mathematical concept that measures the "amount of information"
of a probability distribution. If we consider a variable which takes its values
in the set ${a_1, a_2, \ldots, a_n}$ with the probabilities $p_i = p(a_i)$ for
$i = 1, \ldots, n$, then the entropy $H(X)$ is defined by:
$$H(X) = - \sum\limits_{i = 1}^{n}p_i \log_2 p_i$$

The entropy gives a bound on the average number of bits used to code a symbol.
That is, it is impossible to compress any binary to a size that is smaller than
its entropy times the number of bytes.

In the rest of this document, we will take as an example input text the following
```text
this is an example of a huffman tree
```

You can define it in `huffman.py` and use it later to make sure your implementation is correct.

#### Question 12. [[toc](README.md#table-of-content)]
:pencil: Write a function `compute_distribution` that will compute the
probability distribution of all the characters in a text. It should have the
following signature:
- arguments:
  - `text`: a string containing all the characters of the text, in order
- return: the probability distribution $X$, i.e. the frequencies of appearances,
  of all the symbols in the text, represented as a dictionary where the keys are
  the $(a_i)$ and the values are the associated $(p_i)$

> :bulb: **Note**
>
> another version of this would be to simply compute the number of occurrences, as the rest of the
> Huffman algorithm only needs to be able to sort symbols by "_frequency_" and the two quantities
> are equal up to a multiplicative constant.

:bulb: The distribution for our example text is the following
| symbol | frequency (3 decimals) |
| ------ | ---------------------- |
| t      | $0.056$                |
| h      | $0.056$                |
| i      | $0.056$                |
| s      | $0.056$                |
|        | $0.194$                |
| a      | $0.111$                |
| n      | $0.056$                |
| e      | $0.111$                |
| x      | $0.028$                |
| m      | $0.056$                |
| p      | $0.028$                |
| l      | $0.028$                |
| o      | $0.028$                |
| f      | $0.083$                |
| u      | $0.028$                |
| r      | $0.028$                |

#### Question 13. [[toc](README.md#table-of-content)]
:pencil: Write a function `entropy` that will compute the entropy of a
probability distribution. It should have the following signature:
- arguments:
  - `x`: a dictionary representing the probability distribution $X$, i.e. where
    the keys are the $(a_i)$ and the values are the associated $(p_i)$
- return: the entropy of `x`, i.e. $H(X)$

:bulb: The entropy of our example input is $3.714192447093237$.

:question: What is the entropy of `README.md`?

> :bulb: **Note**
>
> you can use the `read_text` function from `src.helpers` to open text files.

### Huffman trees [[toc](README.md#table-of-content)]

The idea behind _Huffman_ compression is to assign to each symbol in the text a
binary sequence of $0$s and $1$s.

There are two properties that we want to have:
- (1) the more common a symbol in the text, the shorter the encoding. And
  vice-versa with the least common symbols, e.g. in English, "_e_" should have
  the smallest encoding because it is the most common letter and "_z_" should
  have the longest one.
- (2) we want to use an encoding where no encoded symbol is the prefix of
  another one

Intuitively, property (1) will help reduce the size of the text file by
assigning shorter encodings to the most common symbols in the text.

Property (2) will make sure there is no ambiguity in the encoding and greatly
help the decoding process.

In order to achieve this, we will be using a particular tree: the _Huffman_
tree.

The construction of the tree is as follows:
- compute the probability distribution of the symbols in the text
- put all the symbols with their "weights", i.e. their frequencies at the start,
  in a _priority queue_
- pop the two least frequent symbols from the _queue_
- merge them into a tree where they are the two children
- set their "weight" to the sum of the "weights" of the two children
- push this tree back into the queue
- rince and repeat as long as there are strictly more than $1$ item in the _queue_

When there is only one item left in the queue, you can extract it and you should
have the _Huffman_ tree!

#### Question 14. [[toc](README.md#table-of-content)]
:pencil: Write a function called `build_huffman_tree`. It should have the
following signature:
- arguments:
  - `p`: a probability distribution of all the symbols in the text
- return: the _Huffman_, i.e. a nested tuple (see the example tree below for what this can mean)

> :bulb: **None**
>
> the [`src/priority_queue.py`](src/priority_queue.py) provides a _naive_ implementation of a
> _priority queue_ that should be good enough for this class.
>
> you are encouraged to use it or feel free to use the built-in `heapq` module of Python.
>
> there is an example usage of the `src.priority_queue.PriorityQueue` class at the bottom of the module.

:question: Test your `build_huffman_tree` on our example text. Do you get the same tree as below?

> :bulb: **Note**
>
> the weights are expressed as the number of occurrences, which is equivalent to
> the frequencies you have computed, just need to divide / multiply by the total
> number of symbols to get from one to the other.
>
> the weights are here just for reference and easier debugging, they should not
> be there in the final tree output, only the structure of the tree matters, not
> the values in the nodes.

```mermaid
flowchart TD
    %% all the leaves with their respective number of occurrences
    t["'t', 2"]
    h["'h', 2"]
    i["'i', 2"]
    s["'s', 2"]
    spc["' ', 7"]
    a["'a', 4"]
    n["'n', 2"]
    e["'e', 4"]
    x["'x', 1"]
    m["'m', 2"]
    p["'p', 1"]
    l["'l', 1"]
    o["'o', 1"]
    f["'f', 3"]
    u["'u', 1"]
    r["'r', 1"]

    %% intermediate nodes whose value are the sums of the values of their children
    %% these are required because we want the same labels but different Mermaid nodes
    eight1(("8"))
    eight2(("8"))
    eight3(("8"))

    four1(("4"))
    four2(("4"))
    four3(("4"))
    four4(("4"))

    two1(("2"))
    two2(("2"))
    two3(("2"))

    %% depth-first traversal
    36 --- 16
    16 --- eight1
    eight1 --- a
    eight1 --- e
    16 --- eight2
    eight2 --- four1
    four1 --- t
    four1 --- h
    eight2 --- four2
    four2 --- i
    four2 --- s
    36 --- 20
    20 --- eight3
    eight3 --- four3
    four3 --- n
    four3 --- m
    eight3 --- four4
    four4 --- two1
    two1 --- x
    two1 --- p
    four4 --- two2
    two2 --- l
    two2 --- o
    20 --- 12
    12 --- 5
    5 --- two3
    two3 --- u
    two3 --- r
    5 --- f
    12 --- spc
```

In terms of Python code, the your tree might look like the following _nested tuple_

```python
# a nested tuple is simply tuples inside other tuples
(  # this is the root
  (  # this is the left child of the root
    ('a', 'e'),
    (
      ('t', 'h'),   # t and h are siblings
      ('i', 's'),
    ),
  ),
  (  # this is the right child of the root
    (
      ('n', 'm'),
      (
        ('x', 'p'),
        ('l', 'o'),
      ),
    ),
    (
      (
        ('u', 'r'),
        'f',
      ),
      ' ',
   ),
  ),
)
```

> :bulb: **Note**
>
> the exact placement of the nodes in this tree is not important. However, the
> depth at which the nodes are is really important and should depend on the
> frequency of each symbol compared to the other symbols.
>
> e.g. a and e could be swapped, depending on how they are sorted in the _priority queue_,
> however, they need to remain at the same depth, in order to have a encoding of
> the correct length!

### Coding books [[toc](README.md#table-of-content)]

The _Huffman_ tree contains all the information we need about the input text.
However, its shape is not very useful to compress the symbols... We would rather
like something that maps each symbol to the appropriate sequence of $0$s and
$1$s.

This is where dictionaries come into play! We will build a dictionary, from the
tree computed earlier, that will map each symbol to its binary sequence.

The idea is to traverse the tree from the root to each one of the leaves, our
symbols. When going to the left, a $0$ will be added to the sequence, going to
the right will add a $1$.

This will ensure, by construction of the _Huffman_ tree based on the frequencies
of the symbols, that the two _Huffman_ properties defined above are satisfied!

#### Question 15. [[toc](README.md#table-of-content)]
:pencil: Write a function `build_coding_book` that will compute the coding book
of any _Huffman_ tree. It should have the following signature:
- arguments:
  - `t`: the _Huffman_ tree represented as a nested tuple of symbols
- return: the coding book, represented as a dictionary where the keys are the
  symbols and the values are the associated binary sequences

:question: Do you get the expected encoding for all symbols with our example
sentence?

Verify that you get the correct encoding, as below:
```mermaid
flowchart TD
    a["'a', 000"]
    e["'e', 001"]
    t["'t', 0100"]
    h["'h', 0101"]
    i["'i', 0110"]
    s["'s', 0111"]
    n["'n', 1000"]
    m["'m', 1001"]
    x["'x', 10100"]
    p["'p', 10101"]
    l["'l', 10110"]
    o["'o', 10111"]
    u["'u', 11000"]
    r["'r', 11001"]
    f["'f', 1101"]
    spc["' ', 111"]

    n36((" "))
    n16((" "))
    n20((" "))
    n12((" "))
    n5((" "))

    eight1((" "))
    eight2((" "))
    eight3((" "))

    four1((" "))
    four2((" "))
    four3((" "))
    four4((" "))

    two1((" "))
    two2((" "))
    two3((" "))

    %% depth-first traversal
    n36    -- 0 --- n16
    n16    -- 0 --- eight1
    eight1 -- 0 --- a
    eight1 -- 1 --- e
    n16    -- 1 --- eight2
    eight2 -- 0 --- four1
    four1  -- 0 --- t
    four1  -- 1 --- h
    eight2 -- 1 --- four2
    four2  -- 0 --- i
    four2  -- 1 --- s
    n36    -- 1 --- n20
    n20    -- 0 --- eight3
    eight3 -- 0 --- four3
    four3  -- 0 --- n
    four3  -- 1 --- m
    eight3 -- 0 --- four4
    four4  -- 0 --- two1
    two1   -- 0 --- x
    two1   -- 1 --- p
    four4  -- 1 --- two2
    two2   -- 0 --- l
    two2   -- 1 --- o
    n20    -- 1 --- n12
    n12    -- 0 --- n5
    n5     -- 0 --- two3
    two3   -- 0 --- u
    two3   -- 1 --- r
    n5     -- 1 --- f
    n12    -- 1 --- spc
```
Which translates to the following in Python
```python
{
    'a': '000',
    'e': '001',
    't': '0100',
    'h': '0101',
    'i': '0110',
    's': '0111',
    'n': '1000',
    'm': '1001',
    'x': '10100',
    'p': '10101',
    'l': '10110',
    'o': '10111',
    'u': '11000',
    'r': '11001',
    'f': '1101',
    ' ': '111',
}
```

:question: Can you find any encoded sequence of bits that is the prefix of
another one?

> :bulb: **Spoiler**
>
> thanks to property (2) of the _Huffman_ trees, that should be impossible!

### Text compression [[toc](README.md#table-of-content)]

It is now time to put together the compression algorithm. The steps are as follows:
- compute the frequencies of all symbols
- build the _Huffman_ tree
- build the coding book
- build the compressed sequence or $0$s and $1$s by replacing all symbols by
  their associated binary encoding
- convert the sequence to real binary
- return the coding book, the binary sequence and the number of _padding_ bits

But what is _padding_? Well, due to the nature of the _Huffman_ tree, we don't
know what the size of the encodings for each symbol will be. And we know even
less what the final size of the compressed bits will be! In particular, we don't
know if the compressed bits will be a multiple of $8$. To make sure the
compressed data fits perfectly into a whole number of bytes, we'll be adding
some _padding_ bits, e.g. let's say the bits are `0101101011`, we need to add 6
bits of _padding_ to get to 2 bytes, i.e. `0101101011111111` if we pad with $1$s.

#### Question 16. [[toc](README.md#table-of-content)]
:pencil: Write a function `compress` that implements the _Huffman_ compression.
It should have the following signature:
- arguments:
  - `text`: a string containing all the symbols of the input text
- return: the coding book, the encoded binary sequence and the number of padding
  bits

> :bulb: **Note**
>
> you can use the `into_bytes` function from [`src.helpers`](src/helpers.py) to
> convert a string of $0$s and $1$s into real bytes.
>
> there is documentation and you can find some simple examples at the bottom of
> the module.

:question: Test your `compress` function by compressing a bunch of text.

For instance, on our example text from the start, the bits, arranged in groups
of $8$, should be
```
01000101
01100111
11101100
11111100
01000111
00110100
00010011
01011011
00011111
01111101
11100011
10101110
00110111
01100100
01000111
01001100
1001001
```
and there should be 1 bit of padding.

:question: Is the average number of bits used in the compressed output always
higher than the entropy of the input text?

You can plot your results for
- samples of different lengths in a real text file, e.g. written in English to
  use the structure of the language
- sample random character strings of various length, i.e. without structure

### Text decompression [[toc](README.md#table-of-content)]

Having a compressed version of any input text, without any loss, is great, but
it would be great to get back the original text!

In order to achieve that, we need three things
- the decoding book
- the compressed bytes
- the number of padding bits

Luckily, the decoding book is as simple as inverting the keys and the values in
the coding book. This will give us a dictionary that maps the sequences of bits
to their original symbol, i.e. the exact inverse of the coding book.

#### Question 17. [[toc](README.md#table-of-content)]
:pencil: Write a function `inverse_dict` that will inverse the keys and the
values of a dictionary. It should have the following signature:
- arguments:
  - `d`: a dictionary
- return: the same dictionary as `d` but where the keys are now the values and
vice-versa

> :bulb: **Note**
>
> this dictionary inversion only makes sense for dictionaries where all values
> are unique.

#### Question 18. [[toc](README.md#table-of-content)]
:pencil: Write a function `decompress` that implements the decompression. It
should have the following signature:
- arguments:
  - `book`: the codebook that comes from the compression
  - `compressed`: the compressed binary data
  - `padding`: the number of padding bits
- return: the original decompressed text file

> :bulb: **Hint**
>
> First, you can use the `from_bytes` function from [`src.helpers`](src/helpers.py) that will
> convert bytes into their string $0$s and $1$s.
>
> there is documentation and you can find some simple examples at the bottom of
> the module.
>
> Second, you need to iterate over the $0$s and $1$s and read the "decode book"
> to rebuild the original sequence of symbols.

:question: Can you reconstruct our original example text with that last function?

As the first byte of this example is `01000101`, the only symbol in our coding
book, thanks to the first _Huffman_ property, that fits is `t` with encoding
`0100`. So we know the first character is `t`.
Then we can remove the first $4$ bits of the compressed bits and search for the
next symbol.
Without the bits of `t`, the first remaining $8$ bits are `01010110`. We see
that the only encoding that fits these first bits is `0101` and corresponds to
the symbol `h`.

And so on...


### Writing a CLI compression tool [[toc](README.md#table-of-content)]

Finally, we will be writing a little CLI application that will allow us to
compress and decompress any file from the terminal!

There will be three steps
- define the file format we want to use for the compressed files
- write "write" and "read" functions that will use this format
- write a CLI interface to wrap this all up

#### The compression format [[toc](README.md#table-of-content)]
In this application, we will be using a very simple and naive output file
format.
We need to write the codebook, the bytes and the number of padding bits to the
disk. The format can be the following:
- write the codebook as raw JSON on a first line
- add a newline
- write the number of padding bits as a single byte
- add a newline
- write the compressed bytes in the rest of the file

> :bulb: **Note**
>
> the JSON part is really probably not the best...
>
> you can try to find a better file format for that part :wink:

Graphically, a compressed file could look something like the following
```
.-----------------------------.
|        JSON codebook        |
|    .----.---------.----.----|
|    | \n | padding | \n |    |
|----`----'---------'----'    |
|                             |
|                             |
|        encoded binary       |
|                             |
|                             |
`-----------------------------'
```

#### Writing to and reading from the disk [[toc](README.md#table-of-content)]
##### Question 19. [[toc](README.md#table-of-content)]
:pencil: Write a function `write` that will write all compression data to the
disk following the file format above. It should have the following signature:
- arguments:
  - `file`: a filename where to write on the disk
  - `book`: the codebook that comes from the compression
  - `compressed`: the compressed binary data
  - `padding`: the number of padding bits

> :bulb: **Note**
>
> below is a small example of how to write newline-separated binary data to a file
> ```python
> with open("my_file.bin", "wb") as handle:
>     handle.write("hello world\n".encode())
>     handle.write(b"abc")
> ```

##### Question 20. [[toc](README.md#table-of-content)]
:pencil: Write a function `read` that will read compression information from
disk following the file format above. It should have the following signature:
- arguments:
  - `file`: a filename where to read from the disk
- return: the codebook that comes from the compression, the compressed binary
  data and the number of padding bits

> :bulb: **Note**
>
> below is a small example of how to read newline-separated binary data from a file
> ```python
> with open("my_file.bin", 'rb') as handle:
>     hello_world = handle.readline().decode()
>     abc = handle.readline()
> ```

#### Wrapping up in a CLI application [[toc](README.md#table-of-content)]

In order to write a CLI application, we will be using the `argparse` module.
Here is an example application with two subcommands

```python
import argparse

parser = argparse.ArgumentParser("")
subparsers = parser.add_subparsers(title="subcommands", dest="subcommand")

foo_parser = subparsers.add_parser("foo")
foo_parser.add_argument("arg", type=str)

bar_parser = subparsers.add_parser("bar")
bar_parser.add_argument("arg", type=str)

args = parser.parse_args()

if args.subcommand == "foo":
    print(f"foo called with {args.arg}")
elif args.subcommand == "bar":
    print(f"bar called with {args.arg}")
else:
    print("nothing to do")
    exit(0)
```

This allows you to call the source file as
```shell
python my_file.py foo 123
```
or
```shell
python my_file.py bar 456
```

##### Question 21. [[toc](README.md#table-of-content)]
:pencil: In a _main_ Python block at the end of `huffman.py`, adapt the snippet
above and define two subcommands, `compress` and `decompress` that will either
compress or decompress some data.

The `compress` subcommand should take two arguments: the input file to compress
and the name of the output file to write to.

The `decompress` subcommand should take a single argument, the location of the
compressed data and print the decompressed text.

:question: Test your application. Does it works?

:question: Compute the compression rate and the average number of bits per
symbol for a few files. Does you application still satisfy the "entropy bound"?

> :bulb: **Hint**
>
> if you want to count the number of bytes contained in a file `file.txt`, you
> can use the following command:
> ```shell
> wc --bytes file.txt | sed 's/ .*//'
> ```

---
---
> this is the end of the mandatory part :clap: :clap:
> [return to TOC](README.md#table-of-content)
>
> You can continue with the optional parts : 
>
> [go to next](avl.md)
