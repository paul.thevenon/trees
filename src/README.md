# `src.ui.maze`
Below are snippets that show how to define a maze and use the `src.ui.maze` module:

- import the library
```python
from src.ui.maze import Canva
```

- define the maze, it should be a dictionary with the same keys as show below
```python
maze = {
    "width": 32,
    "height": 32,
    "vertices": [],
    "edges": [],
    "size": 25,
}
```

- create the canva that will hold the animation
```python
canva = Canva(
    maze["size"] * maze["width"],
    maze["size"] * maze["height"],
    caption="",
    frame_rate=60,
)
canva.setup()
```

- your "_build_" function that will create the maze should take an optional
  `callback` argument which should be a function that takes a maze as input and
  calls `Canva.step`
```python
lambda m: canva.step(m)
```

- finally, run the main loop
```python
# setup some variables
p = []
complete = False

# loop 'til the end of time
while True:
    # run the animation and get back a signal from the canva
    res = canva.loop(maze, path=p, complete=complete)

    # if the signal is a _falsy_ boolean, then the _path_ and _complete_ should
    # be reset
    if isinstance(res, bool) and not res:
        p = []
        complete = False
        # continue to loop and wait for another input
        continue

    # otherwise, the canva just sent back the positions of the path to compute
    (i_s, j_s), (i_e, j_e) = res
    p = path(
        ...
        callback=lambda m, p, v, n: canva.step(m, p, v, n, complete=False),
    )
    complete = True
```

## key bindings
- _SPACE_ to pause and unpause the animation
- _move the mouse_ to preselect a cell
- _left click_ on two different maze cells to select them
- _right click_ to unselect all the cells
- _ENTER_ to run the path algorithm once the maze generation is done

# `src.ui.qtree`
Below are snippets that show how to define a maze and use the `src.ui.qtree` module:

- import the library
```python
from src.ui.qtree import Canva
```

- setup the canva
```python
canva = Canva(1600, 800, caption='', frame_rate=30)
canva.setup()
```

- create a bunch of points
```python
from random import randint

canva.points = []
for _ in range(1000):
    canva.points.append(Ball(
        randint(0, canva.width),
        randint(0, canva.height),
        3,
    ))
```

- run the animation

```python
# compute the new set of points, e.g. by moving them a bit randomly
def update(points: list) -> list:
    return []


# gives back to the canva the things to show.
# it should be either
# - a list of integers, one for each point, that indicates how many other nodes
# are colliding with a each node
# - a quad-tree and the query range
def f(points: list, mouse: (int, int)):
    ...


canva.loop(update, f)
```
