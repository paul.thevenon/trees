from typing import Any, List, Tuple

Value = Any


class PriorityQueue:
    """
    A naive implementation of a _priority queue_.

    An element is said to have a "_higher priority_" if its value is smaller.
    """

    def __init__(self, values: List[Tuple[float, Value]] = []):
        """
        Initialize a _priority queue_ with optional pre-inserted values.
        """
        self._values = values

    def is_empty(self) -> bool:
        """
        Determine if the _queue_ is empty.
        """
        return len(self._values) == 0

    def __len__(self) -> int:
        """
        Give the number of remaining elements in the _queue_.
        """
        return len(self._values)

    def push(self, v: Tuple[float, Value]):
        """
        Push a value into the _queue_, it will be popped last, unless it has a
        _higher priority_.
        """
        self._values.append(v)

    def pop(self) -> Tuple[float, Value]:
        """
        Pop the element of the _queue_ with the _highest priority_.
        """
        sort_key = [w for w, _ in self._values]
        return self._values.pop(sort_key.index(min(sort_key)))


if __name__ == "__main__":
    from random import randint, random

    q = PriorityQueue()

    print("inserting 10 items with random weigths")
    for _ in range(10):
        x = (random(), randint(0, 100))
        print(x)
        q.push(x)

    print("popping every item out of the queue, in order")
    while not q.is_empty():
        print(q.pop())
