from typing import Iterable


def read_text(filename: str) -> str:
    """
    Read text from a file on disk by taking care of the decoding
    """
    with open(filename, "rb") as handle:
        return handle.read().decode()


def __chunks(it: Iterable, n: int) -> Iterable:
    """Yield successive n-sized chunks from it."""
    for i in range(0, len(it), n):
        yield it[i:i + n]


def into_bytes(bits: str) -> (bytes, int):
    """
    Convert a sequence of 0s and 1s, represented as a simple string, into real
    bytes, possibly with padding

    The padding bits are 0s.
    """
    padding = (8 - len(bits) % 8) % 8

    bin = bytes(map(
        lambda n: int(n, 2),
        __chunks(bits + '0' * padding, 8),
    ))

    return bin, padding


def from_bytes(bin: bytes, padding: int) -> str:
    """
    Convert a sequence of bytes to its string representation with 0s and 1s,
    taking care of padding
    """
    decoded = ''.join(format(b, '#010b').removeprefix("0b") for b in bin)
    return decoded if padding == 0 else decoded[:-padding]


if __name__ == "__main__":
    assert into_bytes("0101") == (b'P', 4)
    assert into_bytes("01000001") == (b'A', 0)
    assert into_bytes("010000010100001") == (b'AB', 1)

    assert "0101" == from_bytes(b'P', 4)
    assert "01000001" == from_bytes(b'A', 0)
    assert "010000010100001" == from_bytes(b'AB', 1)
