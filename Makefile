.PHONY: test-search test-tree test-bst test

test-search:
	pytest tests/test_binary_search.py

test-tree:
	pytest tests/test_binary_tree.py

test-bst:
	pytest tests/test_binary_search_tree.py

test-avl:
	pytest tests/test_balanced_tree.py

test:
	pytest
