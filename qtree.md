## Quad trees [[toc](README.md#table-of-content)] (_optional_)

In this section, we will demonstrate the power of trees by drastically improving
the performance of an $n$-body simulation.
In order to achieve that, we will be using _quad trees_ and split the 2d space
into boxes of equal size, recursively.

### $n$-body collision simulation [[toc](README.md#table-of-content)]

In order to demonstrate the drastic performance improvements given by trees, we
will be writing a small $n$-body collision simulation, i.e. $n$ balls will be
moving around randomly in the canva and we will highlight the balls that are
colliding with others.

#### Question 34. [[toc](README.md#table-of-content)]
:mag: Read and familiarize yourself with the `src.ui.qtree` UI library given with the class
material. The documentation can be found [here](src/README.md#srcuiqtree).

:file_folder: Create a new file called `qtree.py`.

:pencil: Complete the following steps:
- create and setup a `Canva` from the `src.ui.qtree` module.
- because we will be manipulating lots of balls, create a `Ball` class.
  It should have three number fields, the x and y coordinates of its center and
  its radius.
- store a bunch of `Ball`s, e.g. $1000$, in a field `points` of your `canva`

> :bulb: **Note**
>
> you can use the `randint` function from the `random` module to draw random
> integers between $0$ and the dimensions of the canva.
>
> you can set the radius of the balls to $3$.

#### Question 35. [[toc](README.md#table-of-content)]
We would like the balls to move around with time.

:pencil: Write an `update` function that takes and returns a list of `Ball`s,
with all their positions updated. Apply a simple _Brownian_ motion to them, e.g.
by adding a random number between $-1$ and $1$ to each one of their coordinates.

#### Question 36. [[toc](README.md#table-of-content)]
Now, let's compute which balls are colliding with others. For now, we don't have
the choice: for each ball, we need to check all the other balls!

:pencil: Write a function `f` that will take a list of `Ball`s and the position
of the mouse which won't be used for now.

> :bulb: **Note**
>
> see the [doc of `src.ui.qtree`](src/README.md#srcuiqtree) for an example of
> what the signature of `f` should look like.

`f` should return a list of the same size of `canva.points` and where item `i`
is the number of balls the $i$-th ball is colliding with.

:pencil: Run `canva.loop` with `update` and `f`.

:question: What is happening? Is this any good?

### Let's speed things up [[toc](README.md#table-of-content)]

So far, when we want to know the number of balls the $i$-th one is colliding
with, we need to look at all the other balls because we don't have the
information about where the balls are relative to their neighbours, especially
when they are all moving!

However, these balls are living in 2d space. So if a ball is in the top-right
part of the canva, it's probably a good idea to only look at balls in the
top-right part or near its border. If a ball is in the bottom-left part of the
top-right part, there might be even less things to check!

Extending this idea recursively, we are starting to build a _quad tree_. It is
a tree with 4 children, the top-left, top-right, bottom-left and bottom-right
parts.
Because it is part of 2d space, it also needs to keep track of its bounding box,
a rectangle.
It finally has a list of items and a capacity.

#### Question 37. [[toc](README.md#table-of-content)]
:pencil: In the `qtree.py` file, create a new class `QuadTree`. Its constructor
should take a `boundary` and a capacity `n`. It should have the following
fields:
- the `boundary` and the capacity `n`
- a list of `Ball`
- a boolean `divided` telling if the tree has been subdivided
- its four children: `se`, `sw`, `ne` and `nw`, named after cardinal directions
  and set to `None` as default

#### Question 38. [[toc](README.md#table-of-content)]
:pencil: Create a class `Rectangle`. This will be our boundary. A `Rectangle`
should have four number fields: `x`, `y`, `w` and `h`, where $(x, y)$ is the
coordinate of the center of the rectangle, and $w$ and $h$ are the width and
height respectively.

:question: What will the boundary of the root of the quad tree be?

#### Question 39. [[toc](README.md#table-of-content)]
To know if we need to insert a `Ball` in the quad tree based on its
position, we need to able to check if a given `Ball` is inside a `Rectangle`.

:pencil: Add a method `contains` to `Rectangle` that will return `True` if the
argument, a `Ball` is inside, and `False` otherwise.

The insertion of an item in a quad tree is as follows:
- if the item is not contained in the boundary, then there is nothing to do
- otherwise
  - if the number of items in the quad tree is less than the capacity, we can
    add the item to it
  - otherwise
    - if the quad tree has not been yet subdivided, subdivide it
    - insert the item in its children, recursively

The subdividion of a quad tree contains three steps:
- compute the boundaries for all the 4 children
- initialize all children to empty quad trees with the correct boundaries
- mark the parent quad tree as _divided_

#### Question 40. [[toc](README.md#table-of-content)]
:pencil: Write the `insert` method of `QuadTree`. It should take a `Ball` as
argument and implement the algorithm described above.

> :bulb: **Hint**
>
> you can write another method, `QuadTree.subdivide` to help you.

#### Question 41. [[toc](README.md#table-of-content)]
:pencil: Visualize your quad tree to make sure it works as expected.

Write a new function, `f_qtree_viz`, that takes a list of `Ball`s and a mouse
position and returns a quad tree containing all the `Ball`s and `None`. You
should also write an empty `query` method for `QuadTree`, that will take a
boundary, e.g. a `Rectangle` and return a list of `Ball`s. You can simply return
the empty list from `query` and we will get back to it later :relieved:

> :bulb: **Note**
>
> you can set the capacity of the quad tree to $10$.

Call `canva.loop` with `f_qtree_viz` instead of `f`. If you see your quad tree,
congratulations :clap: :clap:

As spoiled by the previous question, the only missing part of our quad tree is
the ability to query the items inside a certain boundary, e.g. a rectangle or
better a circle around a point!

#### Question 42. [[toc](README.md#table-of-content)]
:pencil: Add an `intersects` method to `Rectangle`. It should take another
`Rectangle` and return `True` if they intersect or overlap, and `False`
otherwise.

#### Question 43. [[toc](README.md#table-of-content)]
:pencil: Write the body of the `QuadTree.query` method. The algorithm is as
follows:
- if the query boundary does not intersect with the boundary of the quad tree,
  there are no points in there
- otherwise, add to the list of _found_ items the items from the quad tree that
  are contained in the query boundary
- recursively query the children of the tree if it has been divided

#### Question 44. [[toc](README.md#table-of-content)]
:pencil: Instead of returning a quad tree and `None` from `f_qtree_viz`, return
the same quad tree and a `Rectangle` centered on the mouse. You should see the
quad tree as before, but also the query rectangle and the `Ball`s inside it
highlighted in green.

#### Question 45. [[toc](README.md#table-of-content)]
We can now wrap this all up and speed up our simulations!

:pencil: Copy your `f` function and call it `f_qtree`. Instead of iterating over
all other `Ball`s, build a quad tree containing all the `Ball`s and query a
rectangle around each ball.

:question: Is the simulation running normally now?

---
---
> this is the end of the class :clap: :clap:
> [return to TOC](README.md#table-of-content)