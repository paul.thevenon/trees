### Implementing the BST [[toc](README.md#table-of-content)]

Now that we have seen what trees are and the basic operations on binary trees,
it is time to go back to our original problem.

We will assume that we get elements, i.e. integers, at regular intervals and we
want to manage a data structure that allows us to _quickly_ (1) insert, (2)
search and (3) delete elements.

In order to achieve that, we will add one property to our quite general binary
trees above: the _Binary Search_ property.

:bulb: Let $t = (v, l, r)$ be a tree, all values in $l$ should be stricly
smaller than $v$ and all values in $r$ should be strictly greater than $v$.

And this is the only thing that each operation on a BST should preserve, all
other properties, e.g. the fact that we can search any element in the tree
quickly, will come from it!

#### Question 8. [[toc](README.md#table-of-content)]
:pencil: First, create a new class `BinarySearchTree` in `tree.py`. Because a
BST is also just a tree, this new class should inherit from `BinaryTree`.

:pencil: Overwrite the constructor of `BinarySearchTree`. It should only take a
value as argument and, if the value is not `None`, i.e. the tree is not empty, it
should set the left and right children to empty BSTs.

:gear: You can run `pytest tests/test_binary_search_tree.py`, you should have 3 failing tests.

#### Insertion [[toc](README.md#table-of-content)]
The first operation will be to insert an element $v$ in the tree.

This first algorithm, as can be seen in the animation below, is very
straightforward:
- if the tree is empty, then set its value to $v$ and initialize its children
- if the tree is not empty and holds a value $u$, there are three cases:
  - $v \lt u$, insert $v$ in the left child
  - $v \gt u$, insert $v$ in the right child
  - $v = u$, don't do anything as we won't treat duplicate elements in this
    class

![](assets/insert.mp4)

##### Question 9. [[toc](README.md#table-of-content)]
:pencil: Implement `BinarySearchTree.insert`. This method must take as an argument the `value` to insert.

:question: What is the average complexity of `BinarySearchTree.insert`? You
don't need to prove a precise complexity, just an idea would be enough here as
the computation can be quite... messy :wink:

:gear: Run `pytest tests/test_binary_search_tree.py`. If the "insert" test is green, good job :clap:
:clap:

#### Search [[toc](README.md#table-of-content)]
Next is the main operation of our BSTs and our original question: finding if an
element is in the BST or not!

Thanks to the _Binary search_ property and as can be seen in the following
animation, finding an element $v$ works as follows:
- if the tree is empty, then $v$ is not in the tree
- if the tree is not empty and holds a value $u$, there are three cases
  - $v = u$: then $v$ is in the tree
  - $v \lt u$: we need to look for $v$ in the left child
  - $v \gt u$: we need to look for $v$ in the right child

![](assets/find.mp4)

##### Question 10. [[toc](README.md#table-of-content)]
:pencil: Implement `BinarySearchTree.find`. This method takes as an argument the `value` to find, and return `True` or `False`

:question: What is the average complexity of `BinarySearchTree.find`?

:gear: Run `pytest tests/test_binary_search_tree.py`. If the "find" test is green, good job :clap: :clap:

#### Deletion [[toc](README.md#table-of-content)]
Now, we would like to do a bit more with our BSTs, allowing them to do more than
simply search for elements once they have been inserted.

For instance, we might want to find the smallest element in the BST and then
remove it, that would create a _priority queue_.

For that, we need to be able to remove an element from the BST if it is in there
in the first place, while preserving the _Binary search_ property.

As illustrated in the animation below, there are once again a few cases to
consider:
- if the tree is empty, then there is nothing to do
- if the tree is not empty and holds a value $u$, there are three cases
  - $v \lt u$: try to remove $v$ from the left child
  - $v \gt u$: try to remove $v$ from the right child
  - $v = u$: remove $v$

But the question is: how can we remove the root of a tree and preserve the BST
property?

Well, once again, there are a few cases to look at
- if the tree is a leaf, i.e. with no children, then we can simply set it to the
  empty tree
- if the tree has a single child, then this child should take the place of its
  parent
- if the tree has its two children, we can either
  - find the max $M$ in the left child, set the value of the tree to $M$ and
    remove $M$ from the left child
  - find the min $m$ in the right child, set the value of the tree to $m$ and
    remove $m$ from the right child

![](assets/delete.mp4)

##### Question 11. [[toc](README.md#table-of-content)]
:pencil: Implement `BinarySearchTree.delete`. This method take as argument the `value` to delete.

:question: What is the average complexity of `BinarySearchTree.delete`?

:gear: Run `pytest tests/test_binary_search_tree.py`. If the "delete" test is green, good job :clap:
:clap:

---
---
> [go to next](huffman.md)
