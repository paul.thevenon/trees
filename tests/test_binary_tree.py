def test_is_empty():
    from tree import BinaryTree

    assert BinaryTree().is_empty(), "empty tree should be empty"
    assert not BinaryTree(0).is_empty(), "leaf node should not be empty"
    assert not BinaryTree(
        0, BinaryTree(1), BinaryTree(2)
    ).is_empty(), "non-empty tree should not be empty"


def test_is_leaf():
    from tree import BinaryTree

    assert not BinaryTree().is_leaf(), "empty tree should not be a leaf"
    assert BinaryTree(0).is_leaf(), "leaf node should be a leaf"
    assert not BinaryTree(
        0, BinaryTree(1), BinaryTree(2)
    ).is_leaf(), "non-empty tree should not be a leaf"


def tree():
    from tree import BinaryTree

    return BinaryTree(
        0,
        BinaryTree(
            1,
            BinaryTree(3),
            BinaryTree(4),
        ),
        BinaryTree(
            2,
            BinaryTree(5),
            BinaryTree(6),
        ),
    )


def test_traversal_dfs_prefix():
    res = []
    tree().dfs_prefix(lambda x: res.append(x))
    assert res == [0, 1, 3, 4, 2, 5, 6], "deep prefix traversal is incorrect"


def test_traversal_dfs_infix():
    res = []
    tree().dfs_infix(lambda x: res.append(x))
    assert res == [3, 1, 4, 0, 5, 2, 6], "deep infix traversal is incorrect"


def test_traversal_dfs_postfix():
    res = []
    tree().dfs_postfix(lambda x: res.append(x))
    assert res == [3, 4, 1, 5, 6, 2, 0], "deep postfix traversal is incorrect"


def test_traversal_bfs():
    res = []
    tree().bfs(lambda x: res.append(x))
    assert res == [0, 1, 2, 3, 4, 5, 6], "breadth traversal is incorrect"
