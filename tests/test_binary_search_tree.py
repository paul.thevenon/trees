from random import randint
from copy import deepcopy


def test_insert():
    from tree import BinarySearchTree

    t = BinarySearchTree()
    elements = [randint(0, 1000) for _ in range(100)]
    for v in elements:
        t.insert(v)

    res = []

    def acc(x):
        if x is not None:
            res.append(x)
    t.dfs_infix(acc)
    assert res == list(sorted(set(elements))), "BST insertion is incorrect"


def test_find():
    from tree import BinarySearchTree

    t = BinarySearchTree()
    elements = [2 * randint(0, 1000) for _ in range(100)]
    for v in elements:
        t.insert(v)

    for v in elements:
        assert t.find(v), f"BST search is incorrect, {v} should be in the tree"

    not_inside = [2 * randint(0, 1000) + 1 for _ in range(100)]
    for v in not_inside:
        assert not t.find(v), f"BST search is incorrect, {v} should not be in the tree"


def test_delete():
    from tree import BinarySearchTree

    t = BinarySearchTree()
    elements = [2 * randint(0, 1000) for _ in range(100)]
    for v in elements:
        t.insert(v)

    not_inside = [2 * randint(0, 1000) + 1 for _ in range(100)]
    for v in not_inside:
        prev_t = deepcopy(t)
        prev_res = []
        prev_t.dfs_prefix(lambda x: prev_res.append(x))

        t.delete(v)
        res = []
        t.dfs_prefix(lambda x: res.append(x))

        assert prev_res == res, "removing values not in the BST should not change it"

    for v in elements:
        t.delete(v)
    assert t.is_empty(), "tree should be empty after removing all values"
