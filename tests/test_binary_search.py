from random import randint


def test_binary_search():
    from search import binary_search

    values = [randint(0, 1000) for _ in range(100)]

    for v in values:
        assert binary_search(sorted(values), v), "aie"

    for v in [randint(1000, 2000) for _ in range(100)]:
        assert not binary_search(sorted(values), v), "foo"
